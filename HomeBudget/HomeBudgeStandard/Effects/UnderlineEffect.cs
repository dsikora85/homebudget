﻿using Xamarin.Forms;

namespace HomeBudget.Effects
{
    public class UnderlineEffect : BudgetBaseEffect
    {
        public UnderlineEffect() : base($"{GroupName}.{nameof(UnderlineEffect)}")
        {
        }
    }
}
