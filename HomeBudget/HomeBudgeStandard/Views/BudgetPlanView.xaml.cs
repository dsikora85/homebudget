﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Acr.UserDialogs;
using HomeBudgeStandard.Interfaces;
using HomeBudgeStandard.Views.ViewModels;
using HomeBudget.Code;
using HomeBudget.Converters;
using HomeBudget.Utils;
using Syncfusion.Data;
using Syncfusion.SfDataGrid.XForms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomeBudgeStandard.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BudgetPlanView : ContentPage, IActiveAware
    {
        private BudgetPlanViewModel _viewModel;
        private bool _setupDone;   
        private SfDataGrid _dataGrid;
        private Grid _mainGrid;

        public event EventHandler IsActiveChanged;

        bool _isActive;
        public virtual bool IsActive
        {
            get
            {
                return _isActive;
            }

            set
            {
                if (_isActive != value)
                {
                    _isActive = value;
                }
            }
        }

        public BudgetPlanView ()
		{
            _viewModel = new BudgetPlanViewModel();
            BindingContext = _viewModel;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            if (!IsActive)
            {
                return;
            }

            MainBudget.Instance.BudgetDataChanged -= MarkDataChanged;

            if (MainBudget.Instance.IsDataLoaded && !_setupDone)
            {
                _viewModel.Setup();
                Setup();

            }
            else
            {
                _viewModel.UpdateCharts();
                _viewModel.ForceSwitchChart();
            }

            _setupDone = true;
        }

        protected override void OnDisappearing()
        {
            MainBudget.Instance.BudgetDataChanged += MarkDataChanged;
        }

        private void MarkDataChanged(bool obj)
        {
            _setupDone = false;
        }

        public async Task Activate()
        {
            UserDialogs.Instance.ShowLoading("");

            var dataTemplate = (DataTemplate)Resources["ContentTemplate"];
            View view = null;
            await Task.Factory.StartNew(() =>
            {
                view = (View)dataTemplate.CreateContent();
            });
            Content = view;
            BindingContext = _viewModel;

            SetupVariables();
            CreateDataGrid();

            if (MainBudget.Instance.IsDataLoaded && !_setupDone)
            {
                Setup();
            }
            else
            {
                _viewModel.UpdateCharts();
                _viewModel.ForceSwitchChart();
            }

            _setupDone = true;

            UserDialogs.Instance.HideLoading();
        }

        private void SetupVariables()
        {
            _mainGrid = Content.FindByName<Grid>("mainGrid");
        }

        private void Setup()
        {
            _viewModel.Setup();
        }

        private void CreateDataGrid()
        {
            _dataGrid = new SfDataGrid
            {
                EnableDataVirtualization = true,
                AutoGenerateColumns = false,
                AutoExpandGroups = false,
                AllowGroupExpandCollapse = true,
                LiveDataUpdateMode = LiveDataUpdateMode.AllowSummaryUpdate,
                SelectionMode = Syncfusion.SfDataGrid.XForms.SelectionMode.Single,
                NavigationMode = NavigationMode.Cell,
                EditTapAction = TapAction.OnTap,
                GridStyle = new BudgetDataGridStyle(),
                Margin = new Thickness(12, 0),
            };

            _dataGrid.SortComparers.Add(new SortComparer
            {
                PropertyName = "Category.Name",
                Comparer = new BudgetCategorySortComparer()
            });

            _dataGrid.CurrentCellEndEdit += DataGrid_CurrentCellEndEdit;
            _dataGrid.GroupColumnDescriptions.Add(new GroupColumnDescription
            {
                ColumnName = "Category.Name"
            });

            _dataGrid.CaptionSummaryTemplate = new DataTemplate(() =>
            {
                var stackLayout = new StackLayout { Orientation = StackOrientation.Horizontal, Margin = new Thickness(5, 0) };
                var label = new Label { FontFamily = "FiraSans-Regular.otf#Fira Sans Regular", VerticalTextAlignment = TextAlignment.Center, FontSize = 16, TextColor = Color.Black };

                var icon = new Image { HeightRequest = 25 };
                icon.SetBinding(Image.SourceProperty, new Binding(".", BindingMode.Default, new BudgetGridIconConverter(), _dataGrid));

                var converter = new BudgetDataGridSummaryConverter();
                var binding = new Binding(".", BindingMode.Default, converter, _dataGrid);
                label.SetBinding(Label.TextProperty, binding);

                stackLayout.Children.Add(icon);
                stackLayout.Children.Add(label);

                return new ViewCell { View = stackLayout };
            });

            var gridSummaryRow = new GridGroupSummaryRow
            {
                ShowSummaryInRow = true,
                Title = "{Key}: {Total}",
                SummaryColumns = new ObservableCollection<ISummaryColumn>
                {
                    new GridSummaryColumn
                    {
                        Name = "Total",
                        MappingName="Subcat.Value",
                        SummaryType= SummaryType.Custom,
                        CustomAggregate = new CurrencyDataGridHeader(),
                        Format = "{Currency}"
                    }
                }
            };
            _dataGrid.CaptionSummaryRow = gridSummaryRow;

            _dataGrid.Columns.Add(new GridTextColumn
            {
                MappingName = "Subcat.Name",
                HeaderText = "Kategoria",
                HeaderFont = "FiraSans-Bold.otf#Fira Sans Bold",
                RecordFont = "FiraSans-Regular.otf#Fira Sans Regular",
                HeaderCellTextSize = 16,
                LoadUIView = true,
                ColumnSizer = ColumnSizer.Star,
                //Width = 100
            });

            _dataGrid.Columns.Add(new GridNumericColumn
            {
                MappingName = "SubcatPlanned.Value",
                HeaderText = "Suma",
                AllowEditing = true,
                HeaderFont = "FiraSans-Bold.otf#Fira Sans Bold",
                RecordFont = "FiraSans-Regular.otf#Fira Sans Regular",
                //LoadUIView = true,
                CellTextSize = 14,
                HeaderCellTextSize = 16,
                ColumnSizer = ColumnSizer.Star,

                DisplayBinding = new Binding() { Path = "SubcatPlanned.Value", Converter = new CurrencyValueConverter() }
        });

            _dataGrid.SetBinding(SfDataGrid.ItemsSourceProperty, new Binding(path: nameof(_viewModel.Budget), source: _viewModel));

            var grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(3, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition { Height = 50 });

            grid.Children.Add(_dataGrid);
            var button = new Button { Text = "Użyj w kolejnych miesiącach", Style = (Style)Application.Current.Resources["ButtonStyle"], VerticalOptions = LayoutOptions.End, Margin= new Thickness(12, 3) };
            button.SetBinding(Button.CommandProperty, nameof(_viewModel.SaveCommand));
            grid.Children.Add(button);
            Grid.SetRow(button, 1);

            Grid.SetRow(grid, 2);
            _mainGrid.Children.Add(grid);
        }

        private void DataGrid_CurrentCellEndEdit(object sender, GridCurrentCellEndEditEventArgs e)
        {
            if (e.OldValue is double oldValue && e.NewValue is double newValue && oldValue == newValue)
            {
                return;
            }

            Task.Factory.StartNew(() => MainBudget.Instance.Save());

            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Delay(100);

                _dataGrid.View.TopLevelGroup.UpdateCaptionSummaries();
                _dataGrid.View.Refresh();

                _viewModel.UpdateCharts();
            });
        }
    }
}